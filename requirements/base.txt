aiofiles>=0.7.0
acct>=8.1.0<9.0.0
dict-toolbox>=2.2.0<3.0.0
jinja2
jmespath
pop>=21.0.2
pop-evbus>=6.1.0<7.0.0
pop-config>=9.0.0<10.0.0
pop-loop>=1.0.4<2.0.0
pop-serial>=1.1.0<2.0.0
pop-tree>=9.2.1<10.0.0
PyYaml>=6.0
toml
tqdm
rend>=6.4.2<7.0.0
wheel
