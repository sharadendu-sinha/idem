import pytest


@pytest.mark.asyncio
async def test_parse_index(mock_hub, hub):
    """
    test rend.jinja.parse_nested_key renders correctly with nested keys
    """
    mock_hub.tool.idem.arg_bind_utils.parse_nested_key = (
        hub.tool.idem.arg_bind_utils.parse_nested_key
    )

    ret = mock_hub.tool.idem.arg_bind_utils.parse_nested_key(
        "0['resource_id']['access_id']"
    )
    assert ret == ["0", "resource_id", "access_id"]

    ret = mock_hub.tool.idem.arg_bind_utils.parse_nested_key(
        "0[resource_id][access_id]"
    )
    assert ret == ["0", "resource_id", "access_id"]

    ret = mock_hub.tool.idem.arg_bind_utils.parse_nested_key("test[1][0]")
    assert ret == ["test", "1", "0"]


@pytest.mark.asyncio
async def test_data_lookup(mock_hub, hub):
    """
    test rend.jinja.data_lookup renders correctly with nested keys
    """
    expected_output = "testing_nested_data_lookup"
    mock_hub.tool.idem.arg_bind_utils.data_lookup = (
        hub.tool.idem.arg_bind_utils.data_lookup
    )
    mock_hub.tool.idem.arg_bind_utils.parse_nested_key = (
        hub.tool.idem.arg_bind_utils.parse_nested_key
    )

    state_data = {0: {"resource_id": {"access_id": expected_output}}}
    assert state_data[0]
    ret = mock_hub.tool.idem.arg_bind_utils.data_lookup(
        state_data, "0[resource_id][access_id]"
    )

    assert ret == expected_output
