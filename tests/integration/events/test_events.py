import json
import pathlib
import subprocess
import sys
import tempfile

import pytest


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-exec", "idem-*"], indirect=True)
async def test_exec(hub, runpy, kafka, rabbitmq, event_acct_file):
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "exec",
            "test.ping",
            f"--acct-file={event_acct_file}",
        ],
    )
    assert proc.wait() == 0

    expected = {
        "message": {"result": True, "ret": True},
        "tags": {"ref": "exec.test.ping", "type": "exec-post", "acct_details": None},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-low"], indirect=True)
async def test_low(hub, runpy, kafka, rabbitmq, event_acct_file):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    path = pathlib.Path(fh.name)

    expected = {
        "message": [
            {
                "__id__": "s",
                "__sls__": path.stem,
                "fun": "succeed_without_changes",
                "name": "s",
                "order": 1,
                "state": "test",
            }
        ],
        "tags": {"ref": "idem.run.init.start", "type": "state-low-data"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-high"], indirect=True)
async def test_high(hub, runpy, kafka, rabbitmq, event_acct_file):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    path = pathlib.Path(fh.name)

    expected = {
        "message": {
            "s": {
                "__sls__": path.stem,
                "test": ["succeed_without_changes"],
            }
        },
        "tags": {"ref": "idem.sls_source.init.gather", "type": "state-high-data"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-status"], indirect=True)
async def test_status(hub, runpy, kafka, rabbitmq, event_acct_file):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    await hub.pop.loop.sleep(2)

    for status in ("GATHERING", "COMPILING", "RUNNING", "FINISHED"):
        expected = {
            "message": status,
            "tags": {"ref": "idem.state.update_status", "type": "state-status"},
            "run_name": "cli",
        }

        # Test rabbitmq
        received_message = await rabbitmq.get()
        assert json.loads(received_message.body) == expected

        # Test Kafka
        received_message = await kafka.getone()
        assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-chunk"], indirect=True)
async def test_chunk(hub, runpy, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_with_comment:\n    - comment: asdf")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    path = pathlib.Path(fh.name)

    expected = {
        "message": {
            "__id__": "s",
            "__sls__": path.stem,
            "comment": "asdf",
            "fun": "succeed_with_comment",
            "name": "s",
            "state": "test",
            "order": 100000,
        },
        "tags": {
            "ref": "states.test.succeed_with_comment",
            "type": "state-chunk",
        },
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-state"], indirect=True)
async def test_state_pre_post(
    hub, runpy, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx
):
    acct_data = {"profiles": {"test": {"default": {"account_id": 1234}}}}

    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write(
            f"s:\n  test.succeed_without_changes:\n    - acct_data: {json.dumps(acct_data)}"
        )
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0, proc.stderr

    pathlib.Path(fh.name)

    expected_pre = {
        "message": {
            "s": {
                "test.succeed_without_changes": {
                    "ctx": {
                        "acct_details": {"account_id": 1234},
                        "old_state": None,
                        "run_name": "cli",
                        "test": False,
                        "tag": "test_|-s_|-s_|-succeed_without_changes",
                    },
                    "kwargs": {},
                    "name": "s",
                }
            }
        },
        "run_name": "cli",
        "tags": {
            "ref": "states.test.succeed_without_changes",
            "type": "state-pre",
            "acct_details": {"account_id": 1234},
        },
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_pre

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_pre

    expected_post = {
        "message": {
            "changes": {},
            "comment": "Success!",
            "name": "s",
            "result": True,
        },
        "tags": {
            "ref": "states.test.succeed_without_changes",
            "type": "state-post",
            "acct_details": {"account_id": 1234},
        },
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_post

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_post


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-state"], indirect=True)
async def test_state_exec_pre_post(
    hub, runpy, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx
):
    acct_data = {"profiles": {"test": {"default": {"account_id": 1234}}}}

    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write(
            f"s:\n  exec.run:\n    - acct_data: {json.dumps(acct_data)}\n    - path: test.ping"
        )
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0, proc.stderr

    pathlib.Path(fh.name)

    expected_pre = {
        "message": {"s": {"test.ping": {}}},
        "run_name": "cli",
        "tags": {
            "ref": "exec.test.ping",
            "type": "state-pre",
            "acct_details": {"account_id": 1234},
        },
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_pre

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_pre

    expected_post = {
        "message": {
            "changes": {"new": True},
            "comment": ["idem exec test.ping --acct-profile=default"],
            "name": "s",
            "new_state": True,
            "old_state": None,
            "result": True,
        },
        "tags": {
            "ref": "exec.test.ping",
            "type": "state-post",
            "acct_details": {"account_id": 1234},
        },
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_post

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_post


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-run"], indirect=True)
async def test_state_run(
    hub, runpy, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx
):
    acct_data = {"profiles": {"test": {"default": {"account_id": 1234}}}}

    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write(
            f"s:\n  test.succeed_without_changes:\n    - acct_data: {json.dumps(acct_data)}"
        )
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    pathlib.Path(fh.name)

    expected_post = {
        "message": {
            "__run_num": 1,
            "changes": {},
            "comment": "Success!",
            "esm_tag": "test_|-s_|-s_|-",
            "name": "s",
            "new_state": None,
            "old_state": None,
            "result": True,
            "sls_meta": {"ID_DECS": {}, "SLS": {}},
            "tag": "test_|-s_|-s_|-succeed_without_changes",
        },
        "run_name": "cli",
        "tags": {
            "ref": "states.test.succeed_without_changes",
            "type": "state-result",
            "acct_details": {"account_id": 1234},
        },
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    data = json.loads(received_message.body)
    # We can't mock these values in subprocess call, it's good enough that they can be popped
    data["message"].pop("start_time")
    data["message"].pop("total_seconds")
    assert data == expected_post

    # Test Kafka
    received_message = await kafka.getone()
    data = json.loads(received_message.value)
    # We can't mock these values in subprocess call, it's good enough that they can be popped
    data["message"].pop("start_time")
    data["message"].pop("total_seconds")
    assert data == expected_post
