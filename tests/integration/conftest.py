import unittest.mock as mock

import pop.hub
import pytest


@pytest.fixture
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    with mock.patch("sys.argv", ["state", "thing"]):
        hub.pop.config.load(
            ["idem", "rend", "acct", "evbus"], cli="idem", parse_cli=False
        )
    yield hub


@pytest.fixture(scope="module")
def acct_key():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="crypto")
    yield hub.crypto.fernet.generate_key()
