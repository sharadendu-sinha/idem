vpc:
  nest.test.succeed_with_kwargs_as_changes:
  - resource_id: {{ params.get("vpc_resource_id") }}

vpc1:
  nest.test.succeed_with_kwargs_as_changes:
  - resource_id: {{ params.get("vpc_resource_id1") }}

vp2:
  nest.test.succeed_with_kwargs_as_changes:
  - resource_id: {{ params.get("vpc_resource_id2") }}
