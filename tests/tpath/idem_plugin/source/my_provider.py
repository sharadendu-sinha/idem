import json
from typing import ByteString
from typing import Tuple


__virtualname__ = "my_provider"


def __init__(hub):
    hub.source.my_provider.ACCT = ["my_provider"]


async def cache(
    hub, ctx, protocol: str, source: str, location: str
) -> Tuple[str, ByteString]:
    """
    Read data from my_provider
    :param hub:
    :param ctx: ctx.acct will contain the appropriate credentials to connect to my sls source's sdk
    :param protocol:
    :param source: The url/path/location of this sls source
    :param location: The path/key to access my SLS data within my sls source


    Define what credentials should be used in a profile for "my_provider"

    .. code-block:: sls

        my_provider:
          my_profile:
            my_kwarg_1: my_val_1
            other_arbitrary_kwarg: arbitrary_value
    """
    # Literally test if the location is in the source
    if location not in source:
        return location, b""

    # Pass all the input parameters through to a single hard-coded test.present state
    data = {
        location: {
            "test.present": [
                {
                    "new_state": {
                        "ctx": ctx.acct,
                        "source": source,
                        "protocol": protocol,
                    }
                }
            ]
        }
    }

    # Return the location and encoded yaml (yaml can also read json)
    return location, json.dumps(data).encode()
